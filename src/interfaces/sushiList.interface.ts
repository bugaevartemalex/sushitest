export default interface ICard {
    id: number;
    title: string;
    description: string;
    size: string;
    price: number;
    img?: string;
    contains?: string;
    showModal: boolean;
}
