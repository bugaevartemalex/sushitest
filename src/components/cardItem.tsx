import React, { useState } from 'react';
import ICard from '../interfaces/sushiList.interface';

interface ItemEvent {
    onAdd(counter: number, price: number): void;
    onShow(props: object): void;
}

export const CardItem: React.FC<ICard & ItemEvent> = (props) => {
    //
    const [counter, setCounter] = useState<number>(0);
    //

    const addToCard = (event: React.MouseEvent) => {
        let i: number = 1;
        setCounter(counter + i);
        props.onAdd(counter, props.price);
        props.onShow([]);
    };
    const showInfo = (event: any) => {
        // event.stopImmediatePropagation();
        let target = event.target;
        if (target.tagName !== 'BUTTON') {
            props.onShow(props);
        }
    };
    //
    return (
        <li className="catalog__item">
            <div className="item-card" onClick={showInfo}>
                <div className="item-card__img">
                    <img src={props.img} alt={props.title} />
                </div>
                <div className="item-card__text">
                    <span className="item-card__title">{props.title}</span>
                    <p className="item-card__descr">{props.description}</p>
                    <p className="item-card__size">{props.size}</p>
                    <div className="item-card__bottom">
                        <span className="item-card__price">{props.price} ₽</span>
                        <button type="button" className="item-card__button" onClick={addToCard}>
                            в корзину
                        </button>
                    </div>
                </div>
            </div>
        </li>
    );
};
