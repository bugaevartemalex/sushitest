import React from 'react';
import ICard from '../interfaces/sushiList.interface';

export const Modal: React.FC<ICard> = (info) => {
    const classes = ['modal'];

    if (info.showModal) {
        // classes.push('modal-show');
        document.querySelector('.modal')?.classList.add('modal-show');
    }
    const hidden = () => {
        document.querySelector('.modal')?.classList.remove('modal-show');
    };
    return (
        <div className={classes.join(' ')}>
            <div className="overlay" onClick={hidden}></div>
            <div className="modal__inner">
                <div className="modal__img">
                    <img src="img/filadelfiya-s-krevetkoy_600h600_optimized.png" alt="" />
                </div>
                <div className="modal__info">
                    <span className="modal__title">{info.title}</span>
                    <p className="modal__contain">{info.contains}</p>
                    <div className="modal__bottom">
                        <div className="modal__price">{info.price}₽</div>
                        <div className="modal__button">купить</div>
                    </div>
                </div>
            </div>
        </div>
    );
};
