import React, { useState, useEffect } from 'react';
import { CardItem } from './cardItem';
import { Modal } from './modal';
import ICard from '../interfaces/sushiList.interface';

interface CatalogListProps {
    list: ICard[];
}
export const CatalogList: React.FC<CatalogListProps> = ({ list }) => {
    const [cardCount, setCount] = useState<number[]>([]);
    const [totalPrice, setPrice] = useState<number>(0);
    const [info, setInfo] = useState<any>('0');
    //
    useEffect(() => {
        const saved = JSON.parse(localStorage.getItem('cardCount') || '[]') as number[];
        const savedPrice = JSON.parse(localStorage.getItem('totalPrice') || '0') as number;
        setCount(saved);
        setPrice(savedPrice);
    }, []);
    useEffect(() => {
        localStorage.setItem('cardCount', JSON.stringify(cardCount));
    }, [cardCount]);
    useEffect(() => {
        localStorage.setItem('totalPrice', JSON.stringify(totalPrice));
    }, [totalPrice]);
    //
    const Counter = (counter: number, price: number) => {
        setCount([...cardCount, counter + 1]);
        setPrice(totalPrice + price);
    };
    const ShowModal = (props: object) => {
        setInfo(props);
    };
    if (list.length === 0) {
        return <p>товаров нет</p>;
    }
    return (
        <>
            <main className="main">
                <div className="main__container container">
                    <div className="catalog">
                        <ul className="catalog__list">
                            {list.map((item, index) => (
                                <CardItem {...item} key={index} onAdd={Counter} onShow={ShowModal} />
                            ))}
                        </ul>
                    </div>
                    <div className="basketTest">{cardCount.length} позиции</div>
                    <div className="basketTest">{totalPrice} ₽</div>
                </div>
            </main>
            <Modal {...info} />
        </>
    );
};
