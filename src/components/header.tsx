import React from 'react';
import { useHistory } from 'react-router-dom';

export const Header: React.FC = (props) => {
    const history = useHistory();

    return (
        <header className="header">
            <div className="header__container container">
                <div className="header__inner">
                    <span className="logo" onClick={() => history.push('/')}>
                        <img src="img/logo.png" alt="" />
                    </span>
                    <div className="header__contacts">
                        <ul className="header__list">
                            <li className="header__item">
                                <button className="header__city">Волгоград</button>
                            </li>
                            <li className="header__item">
                                <a href="tel:8-800-550-30-30">8-800-550-30-30</a>
                            </li>
                            <li className="header__item">
                                <a href="/" className="header__account">
                                    Личный кабинет
                                </a>
                            </li>
                        </ul>
                    </div>
                    <nav className="header__menu">
                        <ul className="menu">
                            <li className="menu__item">
                                <a href="/" className="menu__link">
                                    О компании
                                </a>
                            </li>
                            <li className="menu__item">
                                <a href="/" className="menu__link">
                                    Условия доставки
                                </a>
                            </li>
                            <li className="menu__item">
                                <a href="/" className="menu__link">
                                    Пользовательское соглашение
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <button className="header__button">
                        <span>Икринки</span>
                    </button>
                    <a className="header__basket" href="/">
                        <span>1 поз. / 100₽</span>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="26"
                            height="23"
                            className="navbar-nav-item-link-cart-img js-basket-img"
                        >
                            <defs />
                            <path
                                fill="#979797"
                                fillRule="evenodd"
                                d="M8 6.46h10L15.13 1.5a1 1 0 111.74-1l3.44 5.96H24a2 2 0 012 2v12a2 2 0 01-2 2H2a2 2 0 01-2-2v-12c0-1.1.9-2 2-2h3.7L9.12.5a1 1 0 111.74 1L8 6.46zm12 3a1 1 0 00-1 1v8a1 1 0 102 0v-8a1 1 0 00-1-1zm-7 0a1 1 0 00-1 1v8a1 1 0 102 0v-8a1 1 0 00-1-1zm-7 0a1 1 0 00-1 1v8a1 1 0 102 0v-8a1 1 0 00-1-1z"
                            />
                        </svg>
                    </a>
                </div>
            </div>
        </header>
    );
};
