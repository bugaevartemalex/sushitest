import React from 'react';

import { NavLink } from 'react-router-dom';

export const Hero: React.FC = () => {
    const bgStyle = {
        blue: {
            backgroundColor: '#8ed8f7',
        },
        red: {
            backgroundColor: '#f32b2b',
        },
        white: {
            backgroundColor: '#ffffff',
        },
        green: {
            backgroundColor: '#9fe48a',
        },
    };
    return (
        <main className="main">
            <div className="main__container container">
                <div className="main__grid">
                    <section className="main__col" style={bgStyle.blue}>
                        <NavLink className="category category--sets" to="/catalog">
                            <div className="category__img">
                                <img src="img/sets_desktop.png" alt="" />
                            </div>
                            <h2 className="category__title category__title--big">Cэты</h2>
                        </NavLink>
                    </section>
                    <section className="main__col" style={bgStyle.red}>
                        <NavLink className="category category--row category--rolls" to="/catalog">
                            <div className="category__img">
                                <img src="img/rolls_desktop.png" alt="" />
                            </div>
                            <h2 className="category__title">Роллы</h2>
                        </NavLink>
                        <NavLink className="category category--row category--sushi" to="/catalog">
                            <h2 className="category__title">Суши</h2>
                            <div className="category__img">
                                <img src="img/sushi_desktop.png" alt="" />
                            </div>
                        </NavLink>
                        <NavLink className="category category--row category--fried" to="/catalog">
                            <div className="category__img">
                                <img src="img/fried_rolls_desktop.png" alt="" />
                            </div>
                            <h2 className="category__title">Жареные Роллы</h2>
                        </NavLink>
                        <NavLink className="category category--row category--baked" to="/catalog">
                            <h2 className="category__title">Запеченные Роллы</h2>
                            <div className="category__img">
                                <img src="img/baked_rolls_desktop.png" alt="" />
                            </div>
                        </NavLink>
                    </section>
                    <section className="main__col" style={bgStyle.white}>
                        <NavLink className="category category--wok" to="/catalog">
                            <h2 className="category__title category__title--big">
                                Вок конструк
                                <br />
                                тор
                            </h2>
                            <h2 className="category__subtitle">собери свой</h2>
                            <div className="category__img">
                                <img src="img/wok_desktop.png" alt="" />
                            </div>
                        </NavLink>
                    </section>
                    <section className="main__col" style={bgStyle.red}>
                        <NavLink className="category category--soup" to="/catalog">
                            <div className="category__img">
                                <img src="img/soups_desktop.png" alt="" />
                            </div>
                            <h2 className="category__title category__title--big">Супы</h2>
                        </NavLink>
                        <NavLink className="category category--temp" to="/catalog" style={bgStyle.green}>
                            <div className="category__img">
                                <img src="img/tempura_desktop.png" alt="" />
                            </div>
                            <h2 className="category__title">темпура</h2>
                        </NavLink>
                        <NavLink className="category category--drink" to="/catalog" style={bgStyle.green}>
                            <h2 className="category__title">напитки</h2>
                            <div className="category__img">
                                <img src="img/beverages_desktop.png" alt="" />
                            </div>
                        </NavLink>
                    </section>
                </div>
            </div>
        </main>
    );
};
