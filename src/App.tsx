import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Header } from './components/header';
import { Footer } from './components/footer';
import { Main } from './pages/main';
import { Catalog } from './pages/catalog';

const App: React.FC = () => {
    return (
        <BrowserRouter>
            <div className="page-wrapper">
                <div className="content">
                    <Header />
                    <Switch>
                        <Route component={Main} path="/" exact></Route>
                        <Route component={Catalog} path="/catalog"></Route>
                    </Switch>
                </div>
            </div>
            <Footer />
        </BrowserRouter>
    );
};

export default App;
