import React, { useState } from 'react';
import { CatalogList } from '../components/catalogList';

export const Catalog: React.FC = () => {
    const [listSishi] = useState([
        {
            id: 1,
            title: 'Филадельфия с креветкой',
            description: 'креветка тигровая, лосось, огурец, сыр сливочный',
            size: '240 г. 8 шт',
            price: 1,
            img: 'img/filadelfiya-s-krevetkoy_600h600_optimized.png',
            contains: '1.6 белков, 8.0 углеводов, 11 белков, 254 Ккал',
            showModal: true,
        },
        {
            id: 2,
            title: 'Филадельфия',
            description: 'лосось, огурец, сыр сливочный',
            size: '240 г. 8 шт',
            price: 5,
            img: 'img/filadelfiya-s-krevetkoy_600h600_optimized.png',
            contains: '6.6 белков, 8.0 углеводов, 11 белков, 254 Ккал',
            showModal: true,
        },
        {
            id: 3,
            title: 'Филадельфия с копченным лососем',
            description: 'копченный лосось, огурец, сыр сливочный',
            size: '240 г. 8 шт',
            price: 10,
            img: 'img/filadelfiya-s-krevetkoy_600h600_optimized.png',
            contains: '3.6 белков, 8.0 углеводов, 11 белков, 254 Ккал',
            showModal: true,
        },
        {
            id: 4,
            title: 'Филадельфия прадо',
            description: 'икра, лосось, огурец, сыр сливочный',
            size: '240 г. 8 шт',
            price: 20,
            img: 'img/filadelfiya-s-krevetkoy_600h600_optimized.png',
            contains: '4.6 белков, 8.0 углеводов, 11 белков, 254 Ккал',
            showModal: true,
        },
    ]);

    return (
        <>
            <CatalogList list={listSishi} />
        </>
    );
};
